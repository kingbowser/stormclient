package net.minecraft.src;

import java.io.File;
import net.minecraft.client.Minecraft;

public class StormHook
{

    static Minecraft gameMinecraft;

    public StormHook()
    {
    }

    public static boolean isCreative()
    {
        return getPlayer().capabilities.allowFlying;
    }

    public static boolean isFallImmune()
    {
        return isCreative() || getPlayer().isInWater() || getPlayer().isOnLadder() && !getPlayer().onGround;
    }

    public static boolean isGameFocused()
    {
        return gameMinecraft.inGameHasFocus;
    }

    public static boolean isMultiplayer()
    {
        return getWorld().multiplayerWorld;
    }

    public static boolean isSneaking()
    {
        if(StormModFlight.ref.isEnabled())
        {
            return false;
        } else
        {
            return getPlayer().isSneaking();
        }
    }

    public static float[] getBrightnessTable()
    {
        return getWorld().worldProvider.lightBrightnessTable;
    }

    public static float getFallDistance(boolean flag)
    {
        float f = getPlayer().fallDistance - (flag ? 0.0F : 3F);
        if(f < 0.0F)
        {
            return 0.0F;
        }
        if(f < 1.5F && f > 1.0F)
        {
            return 1.5F;
        } else
        {
            return f;
        }
    }

    public static int getFood()
    {
        return getPlayer().foodStats.getFoodLevel();
    }

    public static int getHealth()
    {
        return getPlayer().health;
    }

    public static EntityPlayerSP getPlayer()
    {
        return gameMinecraft.thePlayer;
    }

    public static double getReachDistance()
    {
        if(!isMultiplayer())
        {
            return 64D;
        }
        if(!StormModNoDetect.ref.isEnabled())
        {
            return 6D;
        }
        if(isCreative())
        {
            return 5.6D;
        } else
        {
            return StormModMine.ref.isEnabled() ? 4D : 4.5D;
        }
    }

    public static File getMinecraftDir()
    {
        return gameMinecraft.mcDataDir;
    }

    public static NetClientHandler getSendQueue()
    {
        return isMultiplayer() ? ((EntityClientPlayerMP)getPlayer()).sendQueue : null;
    }

    public static World getWorld()
    {
        return gameMinecraft.theWorld;
    }

    public static void refreshWorld()
    {
        gameMinecraft.renderGlobal.loadRenderers();
    }

    public static void sendChatMessage(String s)
    {
        s = (new StringBuilder()).append("\302\2473[Notification]\302\247f: ").append(s).toString();
        gameMinecraft.ingameGUI.addChatMessage(s);
    }

    public static void setFlying(boolean flag)
    {
        getPlayer().capabilities.isFlying = flag;
    }

    public static void setIceFriction(float f)
    {
        Block.ice.slipperiness = f;
    }

    public static void setSpeed(float f)
    {
        getPlayer().speedOnGround = 0.1F * f;
        getPlayer().speedInAir = 0.02F * f;
    }

    public static void setSprinting(boolean flag)
    {
        getPlayer().setSprinting(flag);
    }

    public static void setStepHeight(float f)
    {
        getPlayer().stepHeight = f;
    }
}
