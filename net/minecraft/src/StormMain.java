// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;

import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.client.Minecraft;

// Referenced classes of package net.minecraft.src:
//            StormModNoDetect, StormModNoFall, StormModMovement, StormModMine, 
//            StormModBrightness, StormModXray, StormModSpeed, StormModFlight, 
//            StormModPillar, StormHook, StormMod

public class StormMain
{

    static final String STORMMODNAME = "Storm Client";
    static final ArrayList stormModList = new ArrayList();
    public static Minecraft mc;

    public StormMain()
    {
    }

    public static void onStart()
    {
        stormModList.add(StormModNoDetect.ref);
        stormModList.add(StormModNoFall.ref);
        stormModList.add(StormModMovement.ref);
        stormModList.add(StormModMine.ref);
        stormModList.add(StormModBrightness.ref);
        stormModList.add(StormModXray.ref);
        stormModList.add(StormModSpeed.ref);
        stormModList.add(StormModFlight.ref);
        stormModList.add(StormModPillar.ref);
        StormModNoFall.ref.toggle();
        StormModMovement.ref.toggle();
        StormModMine.ref.toggle();
    }

    public static void onChangeWorld()
    {
        if(StormHook.getWorld() != null)
        {
            StormMod stormmod;
            for(Iterator iterator = stormModList.iterator(); iterator.hasNext(); stormmod.onChangeWorld())
            {
                stormmod = (StormMod)iterator.next();
            }

        }
    }

    public static void onTick()
    {
        if(StormHook.getWorld() != null)
        {
            StormMod stormmod;
            for(Iterator iterator = stormModList.iterator(); iterator.hasNext(); stormmod.onTick())
            {
                stormmod = (StormMod)iterator.next();
                stormmod.checkInput();
            }

        }
    }

    public static void onRespawn()
    {
        StormMod stormmod;
        for(Iterator iterator = stormModList.iterator(); iterator.hasNext(); stormmod.onRespawn())
        {
            stormmod = (StormMod)iterator.next();
        }

    }

    public static void onNoDetect()
    {
        StormMod stormmod;
        for(Iterator iterator = stormModList.iterator(); iterator.hasNext(); stormmod.onNoDetect())
        {
            stormmod = (StormMod)iterator.next();
        }

    }

    public static boolean onCommand(String s)
    {
        if(!s.startsWith("."))
        {
            return false;
        }
        s = s.trim().substring(1);
        String as[] = s.split(" ");
        for(Iterator iterator = stormModList.iterator(); iterator.hasNext();)
        {
            StormMod stormmod = (StormMod)iterator.next();
            if(stormmod.onCommand(as))
            {
                return true;
            }
        }

        return false;
    }

}
