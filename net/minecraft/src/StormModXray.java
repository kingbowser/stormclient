// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;

import java.util.Arrays;

// Referenced classes of package net.minecraft.src:
//            StormMod, StormHotkey, StormModBrightness, StormHook

public class StormModXray extends StormMod
{

    short modAlphaXray;
    static final StormModXray ref = new StormModXray();
    StormHotkey modPrevKey;
    StormHotkey modNextKey;
    private final int blocksIgnore[] = {
        6, 31, 32, 37, 38, 83, 106, 111
    };
    private final int blocksValue[] = {
        19, 22, 41, 42, 46, 54, 57, 84, 86, 91, 
        92, 95, 99, 100, 103
    };
    private final int blocksFuel[] = {
        5, 16, 17
    };
    private final int blocksIron[] = {
        15, 42
    };
    private final int blocksDiamond[] = {
        10, 11, 56, 57
    };
    private final int blocksGold[] = {
        10, 11, 14, 41
    };
    private final int blocksStructure[] = {
        4, 5, 20, 26, 43, 44, 45, 47, 48, 53, 
        58, 59, 60, 61, 62, 63, 64, 65, 67, 68, 
        71, 84, 85, 91, 96, 98, 101, 102, 107, 108, 
        109
    };
    private final int blocksDungeon[] = {
        48, 52, 54
    };
    private final int blocksCircuit[] = {
        23, 25, 27, 28, 29, 33, 34, 36, 46, 55, 
        66, 69, 70, 71, 72, 73, 74, 75, 76, 77, 
        93, 94
    };
    private final int blocksNether[] = {
        49, 87, 88, 89
    };
    private int blockListCurrent;
    private final int blockLists[][];
    private final String blockListNames[] = {
        "Valuable", "Fuel", "Iron", "Diamond", "Gold", "Structure", "Circuit", "Dungeon", "Nether"
    };

    private StormModXray()
    {
        modAlphaXray = 128;
        blockListCurrent = 0;
        blockLists = (new int[][] {
            blocksValue, blocksFuel, blocksIron, blocksDiamond, blocksGold, blocksStructure, blocksCircuit, blocksDungeon, blocksNether
        });
        modName = (new StringBuilder()).append("X-ray (").append(blockListNames[blockListCurrent]).append(")").toString();
        modPrevKey = new StormHotkey(12);
        modNextKey = new StormHotkey(13);
        modToggleKey = new StormHotkey(45);
    }

    public void onChangeState()
    {
        if(isEnabled())
        {
            StormModBrightness.ref.writeBrightness(1.0F);
        } else
        if(StormModBrightness.ref.isEnabled())
        {
            StormModBrightness.ref.writeBrightness(StormModBrightness.ref.modBrightnessEnabled);
        } else
        {
            StormModBrightness.ref.writeBrightness(0.0F);
        }
        StormHook.refreshWorld();
    }

    public void checkInput()
    {
        if(StormHook.isGameFocused())
        {
            if(modToggleKey.isKeyPressed())
            {
                toggle();
            } else
            if(modNextKey.isKeyPressed())
            {
                nextList();
            } else
            if(modPrevKey.isKeyPressed())
            {
                prevList();
            }
        }
    }

    public boolean isBeingIgnored(int i)
    {
        return Arrays.binarySearch(blocksIgnore, i) >= 0;
    }

    public boolean isInCurrentList(int i)
    {
        return Arrays.binarySearch(blockLists[blockListCurrent], i) >= 0;
    }

    private void nextList()
    {
        blockListCurrent++;
        if(blockListCurrent == blockLists.length)
        {
            blockListCurrent = 0;
        }
        modName = (new StringBuilder()).append("X-ray (").append(blockListNames[blockListCurrent]).append(")").toString();
        if(isEnabled())
        {
            StormHook.refreshWorld();
        }
    }

    private void prevList()
    {
        blockListCurrent--;
        if(blockListCurrent < 0)
        {
            blockListCurrent = blockLists.length - 1;
        }
        modName = (new StringBuilder()).append("X-ray (").append(blockListNames[blockListCurrent]).append(")").toString();
        if(isEnabled())
        {
            StormHook.refreshWorld();
        }
    }

}
