// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            StormMod, StormHotkey, StormModNoDetect, StormHook

public class StormModMovement extends StormMod
{

    private float hackStepEnabled;
    private float hackStepNoDetect;
    static final StormModMovement ref = new StormModMovement();

    private StormModMovement()
    {
        hackStepEnabled = 4F;
        hackStepNoDetect = 1.3F;
        modName = "Movement";
        modToggleKey = new StormHotkey(50);
    }

    public void onChangeState()
    {
        if(isEnabled())
        {
            if(StormModNoDetect.ref.isEnabled())
            {
                StormHook.setStepHeight(hackStepEnabled <= hackStepNoDetect ? hackStepEnabled : hackStepNoDetect);
            } else
            {
                StormHook.setStepHeight(hackStepEnabled);
            }
            StormHook.setIceFriction(0.6F);
            StormHook.setSpeed(1.15F);
        } else
        {
            StormHook.setStepHeight(0.5F);
            StormHook.setIceFriction(0.98F);
        }
    }

    public void onChangeWorld()
    {
        onChangeState();
    }

    public void onRespawn()
    {
        onChangeState();
    }

    public void onNoDetect()
    {
        onChangeState();
    }

    public boolean onCommand(String as[])
    {
        if(as[0].equalsIgnoreCase("sh") || as[0].equalsIgnoreCase("step") || as[0].equalsIgnoreCase("stepheight"))
        {
            if(as.length != 2)
            {
                StormHook.sendChatMessage((new StringBuilder()).append("Step: \302\247a").append(hackStepEnabled).append(" \302\247f: \302\2473[0.1 .. 25]").toString());
            } else
            {
                try
                {
                    hackStepEnabled = Float.parseFloat(as[1]);
                    if(hackStepEnabled < 0.1F)
                    {
                        hackStepEnabled = 0.1F;
                    } else
                    if(hackStepEnabled > 25F)
                    {
                        hackStepEnabled = 10F;
                    }
                    StormHook.sendChatMessage((new StringBuilder()).append("Step: \302\247a").append(hackStepEnabled).append(" \302\247f: \302\2473[0.1 .. 25]").toString());
                    onChangeState();
                }
                catch(NumberFormatException numberformatexception)
                {
                    StormHook.sendChatMessage((new StringBuilder()).append("Speed: \302\247a").append(hackStepEnabled).append(" \302\247f: \302\2473[0.1 .. 25]").toString());
                    StormHook.sendChatMessage("Invalid argument.");
                }
            }
        } else
        {
            return false;
        }
        return true;
    }

}
