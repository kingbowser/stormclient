// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;

import java.io.PrintStream;

// Referenced classes of package net.minecraft.src:
//            StormHook, StormModBrightness

public class StormBrightnessFader extends Thread
{

    private short fadeFramerate;
    private short fadeTime;
    private float fadeBrightnessStart;
    private float fadeBrightnessEnd;

    public StormBrightnessFader(float f)
    {
        fadeFramerate = 45;
        fadeTime = 300;
        fadeBrightnessStart = StormHook.getBrightnessTable()[0];
        fadeBrightnessEnd = f;
    }

    public void run()
    {
        int i = (short)((fadeFramerate * fadeTime) / 1000);
        long l = 1000L / (long)fadeFramerate;
        for(int j = 1; j <= i; j++)
        {
            float f = fadeBrightnessStart + ((float)j * (fadeBrightnessEnd - fadeBrightnessStart)) / (float)i;
            StormModBrightness.ref.writeBrightness(f);
            try
            {
                Thread.sleep(l);
            }
            catch(Exception exception)
            {
                System.out.println("Exception in StormBrightnessFader: Thread.sleep failed!");
            }
        }

    }
}
