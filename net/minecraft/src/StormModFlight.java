// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            StormMod, StormHotkey, StormModNoDetect, StormHook

public class StormModFlight extends StormMod
{

    static final StormModFlight ref = new StormModFlight();

    private StormModFlight()
    {
        modName = "Flight";
        modToggleKey = new StormHotkey(33);
    }

    public void onChangeState()
    {
        if(isEnabled())
        {
            StormHook.setFlying(StormModNoDetect.ref.isEnabled() && (StormHook.isCreative() || !StormHook.isMultiplayer()));
            if(StormHook.isCreative() || !StormModNoDetect.ref.isEnabled() || !StormHook.isMultiplayer())
            {
                modNameColor = 0x13c422;
            } else
            {
                modNameColor = 0x888888;
                StormHook.sendChatMessage("You must disable NoDetect in order to fly.");
            }
        } else
        {
            StormHook.setFlying(false);
        }
    }

    public void onChangeWorld()
    {
        onChangeState();
    }

    public void onRespawn()
    {
        onChangeState();
    }

    public void onNoDetect()
    {
        onChangeState();
    }

}
