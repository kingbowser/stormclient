// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;

import com.pclewis.mcpatcher.mod.TileSize;

// Referenced classes of package net.minecraft.src:
//            TextureFX, Block, MathHelper

public class TextureLavaFX extends TextureFX
{

    protected float field_1147_g[];
    protected float field_1146_h[];
    protected float field_1145_i[];
    protected float field_1144_j[];

    public TextureLavaFX()
    {
        super(Block.lavaMoving.blockIndexInTexture);
        field_1147_g = new float[TileSize.int_numPixels];
        field_1146_h = new float[TileSize.int_numPixels];
        field_1145_i = new float[TileSize.int_numPixels];
        field_1144_j = new float[TileSize.int_numPixels];
    }

    public void onTick()
    {
        for(int i = 0; i < TileSize.int_size; i++)
        {
            for(int j = 0; j < TileSize.int_size; j++)
            {
                float f = 0.0F;
                int l = (int)(MathHelper.sin(((float)j * 3.141593F * 2.0F) / 16F) * 1.2F);
                int i1 = (int)(MathHelper.sin(((float)i * 3.141593F * 2.0F) / 16F) * 1.2F);
                for(int k1 = i - 1; k1 <= i + 1; k1++)
                {
                    for(int i2 = j - 1; i2 <= j + 1; i2++)
                    {
                        int k2 = k1 + l & TileSize.int_sizeMinus1;
                        int i3 = i2 + i1 & TileSize.int_sizeMinus1;
                        f += field_1147_g[k2 + i3 * TileSize.int_size];
                    }

                }

                field_1146_h[i + j * TileSize.int_size] = f / 10F + ((field_1145_i[(i + 0 & TileSize.int_sizeMinus1) + (j + 0 & TileSize.int_sizeMinus1) * TileSize.int_size] + field_1145_i[(i + 1 & TileSize.int_sizeMinus1) + (j + 0 & TileSize.int_sizeMinus1) * TileSize.int_size] + field_1145_i[(i + 1 & TileSize.int_sizeMinus1) + (j + 1 & TileSize.int_sizeMinus1) * TileSize.int_size] + field_1145_i[(i + 0 & TileSize.int_sizeMinus1) + (j + 1 & TileSize.int_sizeMinus1) * TileSize.int_size]) / 4F) * 0.8F;
                field_1145_i[i + j * TileSize.int_size] += field_1144_j[i + j * TileSize.int_size] * 0.01F;
                if(field_1145_i[i + j * TileSize.int_size] < 0.0F)
                {
                    field_1145_i[i + j * TileSize.int_size] = 0.0F;
                }
                field_1144_j[i + j * TileSize.int_size] -= 0.06F;
                if(Math.random() < 0.0050000000000000001D)
                {
                    field_1144_j[i + j * TileSize.int_size] = 1.5F;
                }
            }

        }

        float af[] = field_1146_h;
        field_1146_h = field_1147_g;
        field_1147_g = af;
        for(int k = 0; k < TileSize.int_numPixels; k++)
        {
            float f1 = field_1147_g[k] * 2.0F;
            if(f1 > 1.0F)
            {
                f1 = 1.0F;
            }
            if(f1 < 0.0F)
            {
                f1 = 0.0F;
            }
            float f2 = f1;
            int j1 = (int)(f2 * 100F + 155F);
            int l1 = (int)(f2 * f2 * 255F);
            int j2 = (int)(f2 * f2 * f2 * f2 * 128F);
            if(anaglyphEnabled)
            {
                int l2 = (j1 * 30 + l1 * 59 + j2 * 11) / 100;
                int j3 = (j1 * 30 + l1 * 70) / 100;
                int k3 = (j1 * 30 + j2 * 70) / 100;
                j1 = l2;
                l1 = j3;
                j2 = k3;
            }
            imageData[k * 4 + 0] = (byte)j1;
            imageData[k * 4 + 1] = (byte)l1;
            imageData[k * 4 + 2] = (byte)j2;
            imageData[k * 4 + 3] = -1;
        }

    }
}
