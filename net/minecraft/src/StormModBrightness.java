// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            StormMod, StormHotkey, StormModXray, StormHook, 
//            StormBrightnessFader

public class StormModBrightness extends StormMod
{

    float modBrightnessEnabled;
    static final StormModBrightness ref = new StormModBrightness();
    private float gameBrightness[];
    private float gameBrightnessBackup[];

    private StormModBrightness()
    {
        modBrightnessEnabled = 0.3F;
        modName = "Brightness";
        modToggleKey = new StormHotkey(48);
    }

    public void onChangeState()
    {
        if(!StormModXray.ref.isEnabled())
        {
            writeBrightnessFade();
        }
    }

    public void onChangeWorld()
    {
        gameBrightness = StormHook.getBrightnessTable();
        gameBrightnessBackup = new float[gameBrightness.length];
        System.arraycopy(gameBrightness, 0, gameBrightnessBackup, 0, gameBrightness.length);
        if(StormModXray.ref.isEnabled())
        {
            writeBrightness(1.0F);
        } else
        if(isEnabled())
        {
            writeBrightness(modBrightnessEnabled);
        }
    }

    public void writeBrightness(float f)
    {
        for(int i = 0; i < gameBrightness.length; i++)
        {
            if(gameBrightnessBackup[i] < f)
            {
                gameBrightness[i] = f;
            } else
            {
                gameBrightness[i] = gameBrightnessBackup[i];
            }
        }

    }

    private void writeBrightnessFade()
    {
        StormBrightnessFader stormbrightnessfader;
        if(isEnabled())
        {
            stormbrightnessfader = new StormBrightnessFader(modBrightnessEnabled);
        } else
        {
            stormbrightnessfader = new StormBrightnessFader(0.0F);
        }
        stormbrightnessfader.start();
    }

}
