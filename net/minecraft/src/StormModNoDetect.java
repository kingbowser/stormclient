// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            StormMod, StormHotkey, StormMain, StormHook

public class StormModNoDetect extends StormMod
{

    static final StormModNoDetect ref = new StormModNoDetect();

    private StormModNoDetect()
    {
        modName = "NoDetect";
        modToggleKey = new StormHotkey(49);
    }

    public void onChangeState()
    {
        StormMain.onNoDetect();
    }

    public void onChangeWorld()
    {
        setEnabled(StormHook.isMultiplayer());
    }

}
