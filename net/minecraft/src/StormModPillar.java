// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            StormMod, StormHotkey, GuiIngame, StormHook

public class StormModPillar extends StormMod
{

    private float hackStepEnabled;
    private float hackStepNoDetect;
    static final StormModPillar ref = new StormModPillar();

    private StormModPillar()
    {
        modName = "Pillar";
        modToggleKey = new StormHotkey(51);
    }

    public void onChangeState()
    {
        if(isEnabled())
        {
            GuiIngame.pillar = true;
            StormHook.sendChatMessage("Pillar enabled");
        } else
        {
            GuiIngame.pillar = false;
            StormHook.sendChatMessage("Pillar disabled");
        }
    }

    public void onChangeWorld()
    {
        onChangeState();
    }

    public void onRespawn()
    {
        onChangeState();
    }

    public void onNoDetect()
    {
        onChangeState();
    }

}
