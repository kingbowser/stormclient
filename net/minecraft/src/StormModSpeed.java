// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            StormMod, StormHotkey, StormHook, StormModNoDetect

public class StormModSpeed extends StormMod
{

    public static float speedMultiplier = 2.0F;
    static final StormModSpeed ref = new StormModSpeed();

    private StormModSpeed()
    {
        modToggleKey = new StormHotkey(19);
    }

    public void onChangeState()
    {
        StormHook.setSprinting(false);
        modName = !isEnabled() || StormModNoDetect.ref.isEnabled() ? "Sprint" : "Speed";
    }

    public void onChangeWorld()
    {
        onChangeState();
    }

    public void onRespawn()
    {
        onChangeState();
    }

    public void onNoDetect()
    {
        onChangeState();
    }

    public boolean onCommand(String as[])
    {
        if(as[0].equalsIgnoreCase("speed") || as[0].equalsIgnoreCase("s"))
        {
            if(as.length != 2)
            {
                StormHook.sendChatMessage((new StringBuilder()).append("Speed: \302\247a").append(speedMultiplier).append("x \302\247f: \302\2473[0.1 .. 10]").toString());
            } else
            {
                try
                {
                    speedMultiplier = Float.parseFloat(as[1]);
                    if(speedMultiplier < 0.1F)
                    {
                        speedMultiplier = 0.1F;
                    } else
                    if(speedMultiplier > 10F)
                    {
                        speedMultiplier = 10F;
                    }
                    StormHook.sendChatMessage((new StringBuilder()).append("Speed: \302\247a").append(speedMultiplier).append("x \302\247f: \302\2473[0.1 .. 10]").toString());
                }
                catch(NumberFormatException numberformatexception)
                {
                    StormHook.sendChatMessage((new StringBuilder()).append("Speed: \302\247a").append(speedMultiplier).append("x \302\247f: \302\2473[0.1 .. 10]").toString());
                    StormHook.sendChatMessage("Invalid argument.");
                }
            }
        } else
        {
            return false;
        }
        return true;
    }

}
