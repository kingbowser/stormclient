// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            StormMod, StormHotkey, StormModNoDetect, StormHook, 
//            EntityPlayerSP

public class StormModNoFall extends StormMod
{

    static final StormModNoFall ref = new StormModNoFall();
    float gameFallDistance;
    boolean modNoFall;

    private StormModNoFall()
    {
        gameFallDistance = 0.0F;
        modNoFall = false;
        modName = "NoFall";
        modToggleKey = new StormHotkey(47);
    }

    public void onChangeState()
    {
        gameFallDistance = 0.0F;
    }

    public void onChangeWorld()
    {
        onChangeState();
    }

    public void onRespawn()
    {
        onChangeState();
    }

    public void onTick()
    {
        if(isEnabled())
        {
            if(!StormModNoDetect.ref.isEnabled() || gameFallDistance == 0.0F || !StormHook.isMultiplayer())
            {
                modNameColor = 0x13c422;
            } else
            {
                float f = (float)StormHook.getHealth() - gameFallDistance;
                if(f < 0.0F)
                {
                    f = 0.0F;
                }
                modNameColor = 0xff0000 | (int)(255F * (f / 20F)) << 8;
            }
        }
    }

    public boolean getNoFall()
    {
        if(!isEnabled())
        {
            return StormHook.getPlayer().onGround;
        }
        if(StormModNoDetect.ref.isEnabled())
        {
            return gameFallDistance != 0.0F ? false : StormHook.getPlayer().onGround;
        }
        if(StormHook.getPlayer().onGround)
        {
            return true;
        } else
        {
            modNoFall = !modNoFall;
            return modNoFall;
        }
    }

}
