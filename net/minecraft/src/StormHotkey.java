// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;

import org.lwjgl.input.Keyboard;

public class StormHotkey
{

    public int key;
    private boolean keyState;

    public StormHotkey(int i)
    {
        key = i;
    }

    public boolean isKeyDown()
    {
        return Keyboard.isKeyDown(key);
    }

    public boolean isKeyPressed()
    {
        if(!isKeyDown())
        {
            keyState = false;
            return false;
        }
        if(!keyState)
        {
            keyState = true;
            return true;
        } else
        {
            return false;
        }
    }
}
