// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;

import com.pclewis.mcpatcher.mod.FontUtils;
import com.pclewis.mcpatcher.mod.TextureUtils;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.Random;
import org.lwjgl.opengl.GL11;

// Referenced classes of package net.minecraft.src:
//            GLAllocation, RenderEngine, Tessellator, GameSettings, 
//            ChatAllowedCharacters

public class FontRenderer
{

    private int charWidth[];
    public int fontTextureName;
    public int field_41063_b;
    private int fontDisplayLists;
    private IntBuffer buffer;
    public Random field_41064_c;
    public boolean isUnicode;
    public float charWidthf[];

    public FontRenderer(GameSettings gamesettings, String s, RenderEngine renderengine)
    {
        charWidth = new int[256];
        fontTextureName = 0;
        field_41063_b = 8;
        buffer = GLAllocation.createDirectIntBuffer(1024 /*GL_FRONT_LEFT*/);
        field_41064_c = new Random();
        BufferedImage bufferedimage;
        try
        {
            bufferedimage = TextureUtils.getResourceAsBufferedImage(s);
        }
        catch(IOException ioexception)
        {
            throw new RuntimeException(ioexception);
        }
        int i = bufferedimage.getWidth();
        int j = bufferedimage.getHeight();
        int ai[] = new int[i * j];
        bufferedimage.getRGB(0, 0, i, j, ai, 0, i);
        charWidthf = FontUtils.computeCharWidths(s, bufferedimage, ai, charWidth);
        fontTextureName = renderengine.allocateAndSetupTexture(bufferedimage);
        fontDisplayLists = GLAllocation.generateDisplayLists(288);
        Tessellator tessellator = Tessellator.instance;
        for(int k = 0; k < 256; k++)
        {
            GL11.glNewList(fontDisplayLists + k, 4864 /*GL_COMPILE*/);
            tessellator.startDrawingQuads();
            int i1 = (k % 16) * 8;
            int k1 = (k / 16) * 8;
            float f = 7.99F;
            float f1 = 0.0F;
            float f2 = 0.0F;
            tessellator.addVertexWithUV(0.0D, 0.0F + f, 0.0D, (float)i1 / 128F + f1, ((float)k1 + f) / 128F + f2);
            tessellator.addVertexWithUV(0.0F + f, 0.0F + f, 0.0D, ((float)i1 + f) / 128F + f1, ((float)k1 + f) / 128F + f2);
            tessellator.addVertexWithUV(0.0F + f, 0.0D, 0.0D, ((float)i1 + f) / 128F + f1, (float)k1 / 128F + f2);
            tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, (float)i1 / 128F + f1, (float)k1 / 128F + f2);
            tessellator.draw();
            GL11.glTranslatef(charWidthf[k], 0.0F, 0.0F);
            GL11.glEndList();
        }

        for(int l = 0; l < 32; l++)
        {
            int j1 = (l >> 3 & 1) * 85;
            int l1 = (l >> 2 & 1) * 170 + j1;
            int i2 = (l >> 1 & 1) * 170 + j1;
            int j2 = (l >> 0 & 1) * 170 + j1;
            if(l == 6)
            {
                l1 += 85;
            }
            boolean flag = l >= 16;
            if(gamesettings.anaglyph)
            {
                int k2 = (l1 * 30 + i2 * 59 + j2 * 11) / 100;
                int l2 = (l1 * 30 + i2 * 70) / 100;
                int i3 = (l1 * 30 + j2 * 70) / 100;
                l1 = k2;
                i2 = l2;
                j2 = i3;
            }
            if(flag)
            {
                l1 /= 4;
                i2 /= 4;
                j2 /= 4;
            }
            GL11.glNewList(fontDisplayLists + 256 + l, 4864 /*GL_COMPILE*/);
            GL11.glColor3f((float)l1 / 255F, (float)i2 / 255F, (float)j2 / 255F);
            GL11.glEndList();
        }

    }

    public void drawStringWithShadow(String s, int i, int j, int k)
    {
        renderString(s, i + 1, j + 1, k, true);
        drawString(s, i, j, k);
    }

    public void drawString(String s, int i, int j, int k)
    {
        renderString(s, i, j, k, false);
    }

    public void renderString(String s, int i, int j, int k, boolean flag)
    {
        if(s == null)
        {
            return;
        }
        boolean flag1 = false;
        if(flag)
        {
            int l = k & 0xff000000;
            k = (k & 0xfcfcfc) >> 2;
            k += l;
        }
        GL11.glBindTexture(3553 /*GL_TEXTURE_2D*/, fontTextureName);
        float f = (float)(k >> 16 & 0xff) / 255F;
        float f1 = (float)(k >> 8 & 0xff) / 255F;
        float f2 = (float)(k & 0xff) / 255F;
        float f3 = (float)(k >> 24 & 0xff) / 255F;
        if(f3 == 0.0F)
        {
            f3 = 1.0F;
        }
        GL11.glColor4f(f, f1, f2, f3);
        buffer.clear();
        GL11.glPushMatrix();
        GL11.glTranslatef(i, j, 0.0F);
        for(int i1 = 0; i1 < s.length(); i1++)
        {
            for(; s.length() > i1 + 1 && s.charAt(i1) == '\247'; i1 += 2)
            {
                char c = s.toLowerCase().charAt(i1 + 1);
                if(c == 'k')
                {
                    flag1 = true;
                    continue;
                }
                flag1 = false;
                int k1 = "0123456789abcdef".indexOf(c);
                if(k1 < 0 || k1 > 15)
                {
                    k1 = 15;
                }
                buffer.put(fontDisplayLists + 256 + k1 + (flag ? 16 : 0));
                if(buffer.remaining() == 0)
                {
                    buffer.flip();
                    GL11.glCallLists(buffer);
                    buffer.clear();
                }
            }

            if(i1 < s.length())
            {
                int j1 = ChatAllowedCharacters.allowedCharacters.indexOf(s.charAt(i1));
                if(j1 >= 0)
                {
                    if(flag1)
                    {
                        int l1 = 0;
                        do
                        {
                            l1 = field_41064_c.nextInt(ChatAllowedCharacters.allowedCharacters.length());
                        } while(charWidth[j1 + 32] != charWidth[l1 + 32]);
                        buffer.put(fontDisplayLists + 256 + field_41064_c.nextInt(2) + 8 + (flag ? 16 : 0));
                        buffer.put(fontDisplayLists + l1 + 32);
                    } else
                    {
                        buffer.put(fontDisplayLists + j1 + 32);
                    }
                }
            }
            if(buffer.remaining() == 0)
            {
                buffer.flip();
                GL11.glCallLists(buffer);
                buffer.clear();
            }
        }

        buffer.flip();
        GL11.glCallLists(buffer);
        GL11.glPopMatrix();
    }

    public int getStringWidth(String s)
    {
        if(s == null)
        {
            return 0;
        }
        float f = 0.0F;
        for(int i = 0; i < s.length(); i++)
        {
            if(s.charAt(i) == '\247')
            {
                i++;
                continue;
            }
            int j = ChatAllowedCharacters.allowedCharacters.indexOf(s.charAt(i));
            if(j >= 0)
            {
                f += charWidthf[j + 32];
            }
        }

        return Math.round(f);
    }

    public void drawSplitString(String s, int i, int j, int k, int l)
    {
        func_40609_a(s, i, j, k, l, false);
    }

    public void func_40609_a(String s, int i, int j, int k, int l, boolean flag)
    {
        String as[] = s.split("\n");
        if(as.length > 1)
        {
            for(int i1 = 0; i1 < as.length; i1++)
            {
                drawSplitString(as[i1], i, j, k, l);
                j += splitStringWidth(as[i1], k);
            }

            return;
        }
        String as1[] = s.split(" ");
        int j1 = 0;
        String s1 = "";
        do
        {
            if(j1 >= as1.length)
            {
                break;
            }
            String s2;
            for(s2 = (new StringBuilder()).append(s1).append(as1[j1++]).append(" ").toString(); j1 < as1.length && getStringWidth((new StringBuilder()).append(s2).append(as1[j1]).toString()) < k; s2 = (new StringBuilder()).append(s2).append(as1[j1++]).append(" ").toString()) { }
            int k1;
            for(; getStringWidth(s2) > k; s2 = (new StringBuilder()).append(s1).append(s2.substring(k1)).toString())
            {
                for(k1 = 0; getStringWidth(s2.substring(0, k1 + 1)) <= k; k1++) { }
                if(s2.substring(0, k1).trim().length() <= 0)
                {
                    continue;
                }
                String s3 = s2.substring(0, k1);
                if(s3.lastIndexOf("\247") >= 0)
                {
                    s1 = (new StringBuilder()).append("\247").append(s3.charAt(s3.lastIndexOf("\247") + 1)).toString();
                }
                renderString(s3, i, j, l, flag);
                j += field_41063_b;
            }

            if(getStringWidth(s2.trim()) > 0)
            {
                if(s2.lastIndexOf("\247") >= 0)
                {
                    s1 = (new StringBuilder()).append("\247").append(s2.charAt(s2.lastIndexOf("\247") + 1)).toString();
                }
                renderString(s2, i, j, l, flag);
                j += field_41063_b;
            }
        } while(true);
    }

    public int splitStringWidth(String s, int i)
    {
        String as[] = s.split("\n");
        if(as.length > 1)
        {
            int j = 0;
            for(int k = 0; k < as.length; k++)
            {
                j += splitStringWidth(as[k], i);
            }

            return j;
        }
        String as1[] = s.split(" ");
        int l = 0;
        int i1 = 0;
        do
        {
            if(l >= as1.length)
            {
                break;
            }
            String s1;
            for(s1 = (new StringBuilder()).append(as1[l++]).append(" ").toString(); l < as1.length && getStringWidth((new StringBuilder()).append(s1).append(as1[l]).toString()) < i; s1 = (new StringBuilder()).append(s1).append(as1[l++]).append(" ").toString()) { }
            int j1;
            for(; getStringWidth(s1) > i; s1 = s1.substring(j1))
            {
                for(j1 = 0; getStringWidth(s1.substring(0, j1 + 1)) <= i; j1++) { }
                if(s1.substring(0, j1).trim().length() > 0)
                {
                    i1 += field_41063_b;
                }
            }

            if(s1.trim().length() > 0)
            {
                i1 += field_41063_b;
            }
        } while(true);
        if(i1 < field_41063_b)
        {
            i1 += field_41063_b;
        }
        return i1;
    }

    public void initialize(GameSettings gamesettings, String s, RenderEngine renderengine)
    {
        charWidth = new int[256];
        fontTextureName = 0;
        field_41063_b = 8;
        buffer = GLAllocation.createDirectIntBuffer(1024 /*GL_FRONT_LEFT*/);
        field_41064_c = new Random();
        BufferedImage bufferedimage;
        try
        {
            bufferedimage = TextureUtils.getResourceAsBufferedImage(s);
        }
        catch(IOException ioexception)
        {
            throw new RuntimeException(ioexception);
        }
        int i = bufferedimage.getWidth();
        int j = bufferedimage.getHeight();
        int ai[] = new int[i * j];
        bufferedimage.getRGB(0, 0, i, j, ai, 0, i);
        charWidthf = FontUtils.computeCharWidths(s, bufferedimage, ai, charWidth);
        fontTextureName = renderengine.allocateAndSetupTexture(bufferedimage);
        fontDisplayLists = GLAllocation.generateDisplayLists(288);
        Tessellator tessellator = Tessellator.instance;
        for(int k = 0; k < 256; k++)
        {
            GL11.glNewList(fontDisplayLists + k, 4864 /*GL_COMPILE*/);
            tessellator.startDrawingQuads();
            int i1 = (k % 16) * 8;
            int k1 = (k / 16) * 8;
            float f = 7.99F;
            float f1 = 0.0F;
            float f2 = 0.0F;
            tessellator.addVertexWithUV(0.0D, 0.0F + f, 0.0D, (float)i1 / 128F + f1, ((float)k1 + f) / 128F + f2);
            tessellator.addVertexWithUV(0.0F + f, 0.0F + f, 0.0D, ((float)i1 + f) / 128F + f1, ((float)k1 + f) / 128F + f2);
            tessellator.addVertexWithUV(0.0F + f, 0.0D, 0.0D, ((float)i1 + f) / 128F + f1, (float)k1 / 128F + f2);
            tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, (float)i1 / 128F + f1, (float)k1 / 128F + f2);
            tessellator.draw();
            GL11.glTranslatef(charWidthf[k], 0.0F, 0.0F);
            GL11.glEndList();
        }

        for(int l = 0; l < 32; l++)
        {
            int j1 = (l >> 3 & 1) * 85;
            int l1 = (l >> 2 & 1) * 170 + j1;
            int i2 = (l >> 1 & 1) * 170 + j1;
            int j2 = (l >> 0 & 1) * 170 + j1;
            if(l == 6)
            {
                l1 += 85;
            }
            boolean flag = l >= 16;
            if(gamesettings.anaglyph)
            {
                int k2 = (l1 * 30 + i2 * 59 + j2 * 11) / 100;
                int l2 = (l1 * 30 + i2 * 70) / 100;
                int i3 = (l1 * 30 + j2 * 70) / 100;
                l1 = k2;
                i2 = l2;
                j2 = i3;
            }
            if(flag)
            {
                l1 /= 4;
                i2 /= 4;
                j2 /= 4;
            }
            GL11.glNewList(fontDisplayLists + 256 + l, 4864 /*GL_COMPILE*/);
            GL11.glColor3f((float)l1 / 255F, (float)i2 / 255F, (float)j2 / 255F);
            GL11.glEndList();
        }

    }
}
