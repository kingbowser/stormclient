// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package net.minecraft.src;


// Referenced classes of package net.minecraft.src:
//            StormHook, StormHotkey

public abstract class StormMod
{

    String modName;
    int modNameColor;
    StormHotkey modToggleKey;
    private boolean modEnabled;

    public StormMod()
    {
        modNameColor = 0x13c422;
    }

    public void onChangeState()
    {
    }

    public void onChangeWorld()
    {
    }

    public void onRespawn()
    {
    }

    public void onNoDetect()
    {
    }

    public void onTick()
    {
    }

    public boolean onCommand(String as[])
    {
        return false;
    }

    public void checkInput()
    {
        if(StormHook.isGameFocused() && modToggleKey != null && modToggleKey.isKeyPressed())
        {
            toggle();
        }
    }

    public boolean isEnabled()
    {
        return modEnabled;
    }

    public void setEnabled(boolean flag)
    {
        modEnabled = flag;
        if(StormHook.getWorld() != null)
        {
            onChangeState();
        }
    }

    public void toggle()
    {
        setEnabled(!modEnabled);
    }
}
