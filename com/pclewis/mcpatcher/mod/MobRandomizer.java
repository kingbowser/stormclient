// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package com.pclewis.mcpatcher.mod;

import com.pclewis.mcpatcher.MCPatcherUtils;
import java.util.ArrayList;
import java.util.HashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class MobRandomizer
{

    private static final HashMap mobHash = new HashMap();
    private static TexturePackBase lastTexturePack;
    private static final long MULTIPLIER = 0x5deece66dL;
    private static final long ADDEND = 11L;
    private static final long MASK = 0xffffffffffffL;

    public MobRandomizer()
    {
    }

    public static void reset()
    {
        MCPatcherUtils.log("reset random mobs list", new Object[0]);
        mobHash.clear();
    }

    public static String randomTexture(Entity entity)
    {
        return randomTexture(entity, entity.getEntityTexture());
    }

    public static String randomTexture(Entity entity, String s)
    {
        TexturePackBase texturepackbase = MCPatcherUtils.getMinecraft().texturePackList.selectedTexturePack;
        if(lastTexturePack != texturepackbase)
        {
            lastTexturePack = texturepackbase;
            reset();
        }
        if(lastTexturePack == null || !s.startsWith("/mob/") || !s.endsWith(".png"))
        {
            return s;
        }
        ArrayList arraylist = (ArrayList)mobHash.get(s);
        if(arraylist == null)
        {
            arraylist = new ArrayList();
            arraylist.add(s);
            int i = 2;
            do
            {
                String s1 = s.replace(".png", (new StringBuilder()).append("").append(i).append(".png").toString());
                boolean flag = false;
                java.io.InputStream inputstream = null;
                try
                {
                    inputstream = lastTexturePack.getResourceAsStream(s1);
                    if(inputstream != null)
                    {
                        flag = true;
                    }
                }
                catch(Throwable throwable) { }
                finally
                {
                    MCPatcherUtils.close(inputstream);
                }
                if(!flag)
                {
                    break;
                }
                arraylist.add(s1);
                i++;
            } while(true);
            if(arraylist.size() > 1)
            {
                MCPatcherUtils.log("found %d variations for %s", new Object[] {
                    Integer.valueOf(arraylist.size()), s
                });
            }
            mobHash.put(s, arraylist);
        }
        if(!entity.randomMobsSkinSet)
        {
            entity.randomMobsSkin = getSkinId(entity.entityId);
            entity.randomMobsSkinSet = true;
        }
        int j = (int)(entity.randomMobsSkin % (long)arraylist.size());
        if(j < 0)
        {
            j += arraylist.size();
        }
        return (String)arraylist.get(j);
    }

    private static long getSkinId(int i)
    {
        long l = i;
        l = l ^ l << 16 ^ l << 32 ^ l << 48;
        l = 0x5deece66dL * l + 11L;
        l = 0x5deece66dL * l + 11L;
        l &= 0xffffffffffffL;
        return l >> 32 ^ l;
    }

}
