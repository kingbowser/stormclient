// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package com.pclewis.mcpatcher.mod;

import com.pclewis.mcpatcher.MCPatcherUtils;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.*;

public class FontUtils
{

    private static final int ROWS = 16;
    private static final int COLS = 16;
    public static final char AVERAGE_CHARS[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123467890".toCharArray();
    public static final int SPACERS[] = {
        0x2028bfe, 0x2808080, 0xdffffff
    };
    private static final boolean showLines = false;
    private static Method getResource;

    public FontUtils()
    {
    }

    public static float[] computeCharWidths(String s, BufferedImage bufferedimage, int ai[], int ai1[])
    {
        MCPatcherUtils.log("computeCharWidths(%s)", new Object[] {
            s
        });
        float af[] = new float[ai1.length];
        int i = bufferedimage.getWidth();
        int j = bufferedimage.getHeight();
        int k = i / 16;
        int l = j / 16;
label0:
        for(int i1 = 0; i1 < ai1.length; i1++)
        {
            int k1 = i1 / 16;
            int i2 = i1 % 16;
            int j2 = k - 1;
            do
            {
                if(j2 < 0)
                {
                    continue label0;
                }
                int k2 = i2 * k + j2;
                for(int l2 = 0; l2 < l; l2++)
                {
                    int i3 = k1 * l + l2;
                    int j3 = ai[k2 + i3 * i];
                    if(isOpaque(j3))
                    {
                        if(printThis(i1))
                        {
                            MCPatcherUtils.log("'%c' pixel (%d, %d) = %08x, colIdx = %d", new Object[] {
                                Character.valueOf((char)i1), Integer.valueOf(k2), Integer.valueOf(i3), Integer.valueOf(j3), Integer.valueOf(j2)
                            });
                        }
                        af[i1] = (128F * (float)(j2 + 1)) / (float)i + 1.0F;
                        continue label0;
                    }
                }

                j2--;
            } while(true);
        }

        for(int j1 = 0; j1 < af.length; j1++)
        {
            if(af[j1] <= 0.0F)
            {
                af[j1] = 2.0F;
            }
        }

        boolean aflag[] = new boolean[ai1.length];
        try
        {
            getCharWidthOverrides(s, af, aflag);
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace();
        }
        if(!aflag[32])
        {
            af[32] = defaultSpaceWidth(af);
        }
        for(int l1 = 0; l1 < ai1.length; l1++)
        {
            ai1[l1] = Math.round(af[l1]);
            if(printThis(l1))
            {
                MCPatcherUtils.log("charWidth['%c'] = %f", new Object[] {
                    Character.valueOf((char)l1), Float.valueOf(af[l1])
                });
            }
        }

        return af;
    }

    private static boolean isOpaque(int i)
    {
        int ai[] = SPACERS;
        int j = ai.length;
        for(int k = 0; k < j; k++)
        {
            int l = ai[k];
            if(i == l)
            {
                return false;
            }
        }

        return (i & 0xff) > 0;
    }

    private static boolean printThis(int i)
    {
        return "ABCDEF abcdef".indexOf(i) >= 0;
    }

    private static float defaultSpaceWidth(float af[])
    {
        float f = 0.0F;
        int i = 0;
        char ac[] = AVERAGE_CHARS;
        int j = ac.length;
        for(int k = 0; k < j; k++)
        {
            char c = ac[k];
            if(af[c] > 0.0F)
            {
                f += af[c];
                i++;
            }
        }

        if(i > 0)
        {
            return (f / (float)i) * 0.5F;
        } else
        {
            return 4F;
        }
    }

    private static void getCharWidthOverrides(String s, float af[], boolean aflag[])
    {
        if(getResource == null)
        {
            return;
        }
        String s1 = s.replace(".png", ".properties");
        InputStream inputstream;
        try
        {
            Object obj = getResource.invoke(null, new Object[] {
                s1
            });
            if(!(obj instanceof InputStream))
            {
                return;
            }
            inputstream = (InputStream)obj;
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            return;
        }
        MCPatcherUtils.log("reading character widths from %s", new Object[] {
            s1
        });
        try
        {
            Properties properties = new Properties();
            properties.load(inputstream);
            Iterator iterator = properties.entrySet().iterator();
            do
            {
                if(!iterator.hasNext())
                {
                    break;
                }
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
                String s2 = entry.getKey().toString().trim();
                String s3 = entry.getValue().toString().trim();
                if(s2.matches("^width\\.\\d+$") && !s3.equals(""))
                {
                    try
                    {
                        int i = Integer.parseInt(s2.substring(6));
                        float f = Float.parseFloat(s3);
                        if(i >= 0 && i < af.length)
                        {
                            MCPatcherUtils.log("    setting charWidthf[%d] to %f", new Object[] {
                                Integer.valueOf(i), Float.valueOf(f)
                            });
                            af[i] = f;
                            aflag[i] = true;
                        }
                    }
                    catch(NumberFormatException numberformatexception) { }
                }
            } while(true);
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
        }
        finally
        {
            MCPatcherUtils.close(inputstream);
        }
    }

    static Class _mthclass$(String s)
    {
        try
        {
            return Class.forName(s);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            throw new NoClassDefFoundError(classnotfoundexception.getMessage());
        }
    }

    static 
    {
        try
        {
            Class class1 = Class.forName("com.pclewis.mcpatcher.mod.TextureUtils");
            try
            {
                getResource = class1.getDeclaredMethod("getResourceAsStream", new Class[] {
                    java.lang.String.class
                });
            }
            catch(NoSuchMethodException nosuchmethodexception)
            {
                nosuchmethodexception.printStackTrace();
            }
        }
        catch(ClassNotFoundException classnotfoundexception) { }
    }
}
