// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package com.pclewis.mcpatcher.mod;

import com.pclewis.mcpatcher.MCPatcherUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipFile;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.src.ColorizerFoliage;
import net.minecraft.src.ColorizerGrass;
import net.minecraft.src.ColorizerWater;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.GLAllocation;
import net.minecraft.src.RenderEngine;
import net.minecraft.src.TextureCompassFX;
import net.minecraft.src.TextureFX;
import net.minecraft.src.TextureFlamesFX;
import net.minecraft.src.TextureLavaFX;
import net.minecraft.src.TextureLavaFlowFX;
import net.minecraft.src.TexturePackBase;
import net.minecraft.src.TexturePackCustom;
import net.minecraft.src.TexturePackDefault;
import net.minecraft.src.TexturePackList;
import net.minecraft.src.TexturePortalFX;
import net.minecraft.src.TextureWatchFX;
import net.minecraft.src.TextureWaterFX;
import net.minecraft.src.TextureWaterFlowFX;

// Referenced classes of package com.pclewis.mcpatcher.mod:
//            TileSize, CustomAnimation

public class TextureUtils
{

    private static boolean animatedFire = MCPatcherUtils.getBoolean("HD Textures", "animatedFire", true);
    private static boolean animatedLava = MCPatcherUtils.getBoolean("HD Textures", "animatedLava", true);
    private static boolean animatedWater = MCPatcherUtils.getBoolean("HD Textures", "animatedWater", true);
    private static boolean animatedPortal = MCPatcherUtils.getBoolean("HD Textures", "animatedPortal", true);
    private static boolean customFire = MCPatcherUtils.getBoolean("HD Textures", "customFire", true);
    private static boolean customLava = MCPatcherUtils.getBoolean("HD Textures", "customLava", true);
    private static boolean customWater = MCPatcherUtils.getBoolean("HD Textures", "customWater", true);
    private static boolean customPortal = MCPatcherUtils.getBoolean("HD Textures", "customPortal", true);
    private static boolean customOther = MCPatcherUtils.getBoolean("HD Textures", "customOther", true);
    public static final int LAVA_STILL_TEXTURE_INDEX = 237;
    public static final int LAVA_FLOWING_TEXTURE_INDEX = 238;
    public static final int WATER_STILL_TEXTURE_INDEX = 205;
    public static final int WATER_FLOWING_TEXTURE_INDEX = 206;
    public static final int FIRE_E_W_TEXTURE_INDEX = 31;
    public static final int FIRE_N_S_TEXTURE_INDEX = 47;
    public static final int PORTAL_TEXTURE_INDEX = 14;
    private static HashMap expectedColumns;
    private static boolean useTextureCache = MCPatcherUtils.getBoolean("HD Textures", "useTextureCache", false);
    private static boolean reclaimGLMemory = MCPatcherUtils.getBoolean("HD Textures", "reclaimGLMemory", false);
    private static boolean autoRefreshTextures = MCPatcherUtils.getBoolean("HD Textures", "autoRefreshTextures", false);
    private static TexturePackBase lastTexturePack = null;
    private static HashMap cache = new HashMap();
    private static int textureRefreshCount;

    public TextureUtils()
    {
    }

    public static boolean setTileSize()
    {
        MCPatcherUtils.log("\nchanging skin to %s", new Object[] {
            getTexturePackName(getSelectedTexturePack())
        });
        int i = getTileSize();
        if(i == TileSize.int_size)
        {
            MCPatcherUtils.log("tile size %d unchanged", new Object[] {
                Integer.valueOf(i)
            });
            return false;
        } else
        {
            MCPatcherUtils.log("setting tile size to %d (was %d)", new Object[] {
                Integer.valueOf(i), Integer.valueOf(TileSize.int_size)
            });
            TileSize.setTileSize(i);
            return true;
        }
    }

    private static void setFontRenderer(Minecraft minecraft, FontRenderer fontrenderer, String s)
    {
        boolean flag = fontrenderer.isUnicode;
        fontrenderer.initialize(minecraft.gameSettings, s, minecraft.renderEngine);
        fontrenderer.isUnicode = flag;
    }

    public static void setFontRenderer()
    {
        MCPatcherUtils.log("setFontRenderer()", new Object[0]);
        Minecraft minecraft = MCPatcherUtils.getMinecraft();
        setFontRenderer(minecraft, minecraft.fontRenderer, "/font/default.png");
        if(minecraft.standardGalacticFontRenderer != minecraft.fontRenderer)
        {
            setFontRenderer(minecraft, minecraft.standardGalacticFontRenderer, "/font/alternate.png");
        }
    }

    public static void registerTextureFX(List list, TextureFX texturefx)
    {
        TextureFX texturefx1 = refreshTextureFX(texturefx);
        if(texturefx1 != null)
        {
            MCPatcherUtils.log("registering new TextureFX class %s", new Object[] {
                texturefx.getClass().getName()
            });
            list.add(texturefx1);
            texturefx1.onTick();
        }
    }

    private static TextureFX refreshTextureFX(TextureFX texturefx)
    {
        if((texturefx instanceof TextureCompassFX) || (texturefx instanceof TextureWatchFX) || (texturefx instanceof TextureLavaFX) || (texturefx instanceof TextureLavaFlowFX) || (texturefx instanceof TextureWaterFX) || (texturefx instanceof TextureWaterFlowFX) || (texturefx instanceof TextureFlamesFX) || (texturefx instanceof TexturePortalFX) || (texturefx instanceof CustomAnimation))
        {
            return null;
        }
        System.out.printf("attempting to refresh unknown animation %s\n", new Object[] {
            texturefx.getClass().getName()
        });
        Minecraft minecraft = MCPatcherUtils.getMinecraft();
        Class class1 = texturefx.getClass();
        for(int i = 0; i < 3; i++)
        {
            try
            {
                switch(i)
                {
                case 0: // '\0'
                    Constructor constructor = class1.getConstructor(new Class[] {
                        net.minecraft.client.Minecraft.class, Integer.TYPE
                    });
                    return (TextureFX)constructor.newInstance(new Object[] {
                        minecraft, Integer.valueOf(TileSize.int_size)
                    });

                case 1: // '\001'
                    Constructor constructor1 = class1.getConstructor(new Class[] {
                        net.minecraft.client.Minecraft.class
                    });
                    return (TextureFX)constructor1.newInstance(new Object[] {
                        minecraft
                    });

                case 2: // '\002'
                    Constructor constructor2 = class1.getConstructor(new Class[0]);
                    return (TextureFX)constructor2.newInstance(new Object[0]);
                }
            }
            catch(NoSuchMethodException nosuchmethodexception) { }
            catch(IllegalAccessException illegalaccessexception) { }
            catch(Exception exception)
            {
                exception.printStackTrace();
            }
        }

        if(texturefx.imageData.length != TileSize.int_numBytes)
        {
            MCPatcherUtils.log("resizing %s buffer from %d to %d bytes", new Object[] {
                class1.getName(), Integer.valueOf(texturefx.imageData.length), Integer.valueOf(TileSize.int_numBytes)
            });
            texturefx.imageData = new byte[TileSize.int_numBytes];
        }
        return texturefx;
    }

    public static void refreshTextureFX(List list)
    {
        MCPatcherUtils.log("refreshTextureFX()", new Object[0]);
        ArrayList arraylist = new ArrayList();
        Object obj = list.iterator();
        do
        {
            if(!((Iterator) (obj)).hasNext())
            {
                break;
            }
            TextureFX texturefx = (TextureFX)((Iterator) (obj)).next();
            TextureFX texturefx1 = refreshTextureFX(texturefx);
            if(texturefx1 != null)
            {
                arraylist.add(texturefx1);
            }
        } while(true);
        list.clear();
        obj = MCPatcherUtils.getMinecraft();
        list.add(new TextureCompassFX(((Minecraft) (obj))));
        list.add(new TextureWatchFX(((Minecraft) (obj))));
        TexturePackBase texturepackbase = getSelectedTexturePack();
        boolean flag = texturepackbase == null || (texturepackbase instanceof TexturePackDefault);
        if(!flag && customLava)
        {
            list.add(new CustomAnimation(237, 0, 1, "lava_still", -1, -1));
            list.add(new CustomAnimation(238, 0, 2, "lava_flowing", 3, 6));
        } else
        if(animatedLava)
        {
            list.add(new TextureLavaFX());
            list.add(new TextureLavaFlowFX());
        }
        if(!flag && customWater)
        {
            list.add(new CustomAnimation(205, 0, 1, "water_still", -1, -1));
            list.add(new CustomAnimation(206, 0, 2, "water_flowing", 0, 0));
        } else
        if(animatedWater)
        {
            list.add(new TextureWaterFX());
            list.add(new TextureWaterFlowFX());
        }
        if(!flag && customFire && hasResource("/custom_fire_e_w.png") && hasResource("/custom_fire_n_s.png"))
        {
            list.add(new CustomAnimation(47, 0, 1, "fire_n_s", 2, 4));
            list.add(new CustomAnimation(31, 0, 1, "fire_e_w", 2, 4));
        } else
        if(animatedFire)
        {
            list.add(new TextureFlamesFX(0));
            list.add(new TextureFlamesFX(1));
        }
        if(!flag && customPortal && hasResource("/custom_portal.png"))
        {
            list.add(new CustomAnimation(14, 0, 1, "portal", -1, -1));
        } else
        if(animatedPortal)
        {
            list.add(new TexturePortalFX());
        }
        if(customOther)
        {
            for(int i = 0; i < 2; i++)
            {
                String s = i != 0 ? "item" : "terrain";
                for(int j = 0; j < 256; j++)
                {
                    String s1 = (new StringBuilder()).append("/custom_").append(s).append("_").append(j).append(".png").toString();
                    if(hasResource(s1))
                    {
                        list.add(new CustomAnimation(j, i, 1, (new StringBuilder()).append(s).append("_").append(j).toString(), 2, 4));
                    }
                }

            }

        }
        TextureFX texturefx2;
        for(Iterator iterator = arraylist.iterator(); iterator.hasNext(); list.add(texturefx2))
        {
            texturefx2 = (TextureFX)iterator.next();
        }

        TextureFX texturefx3;
        for(Iterator iterator1 = list.iterator(); iterator1.hasNext(); texturefx3.onTick())
        {
            texturefx3 = (TextureFX)iterator1.next();
        }

        if(ColorizerWater.waterBuffer != ColorizerFoliage.foliageBuffer)
        {
            refreshColorizer(ColorizerWater.waterBuffer, "/misc/watercolor.png");
        }
        refreshColorizer(ColorizerGrass.grassBuffer, "/misc/grasscolor.png");
        refreshColorizer(ColorizerFoliage.foliageBuffer, "/misc/foliagecolor.png");
        System.gc();
    }

    public static TexturePackBase getSelectedTexturePack()
    {
        Minecraft minecraft = MCPatcherUtils.getMinecraft();
        return minecraft != null ? minecraft.texturePackList != null ? minecraft.texturePackList.selectedTexturePack : null : null;
    }

    public static String getTexturePackName(TexturePackBase texturepackbase)
    {
        return texturepackbase != null ? texturepackbase.texturePackFileName : "Default";
    }

    public static ByteBuffer getByteBuffer(ByteBuffer bytebuffer, byte abyte0[])
    {
        bytebuffer.clear();
        int i = bytebuffer.capacity();
        int j = abyte0.length;
        if(j > i || reclaimGLMemory && i >= 4 * j)
        {
            bytebuffer = GLAllocation.createDirectByteBuffer(j);
        }
        bytebuffer.put(abyte0);
        bytebuffer.position(0).limit(j);
        TileSize.int_glBufferSize = j;
        return bytebuffer;
    }

    public static boolean isRequiredResource(String s)
    {
        return !s.startsWith("/custom_") && !s.equals("/terrain_nh.png") && !s.equals("/terrain_s.png") && !s.matches("^/font/.*\\.properties$") && !s.matches("^/mob/.*\\d+.png$");
    }

    public static InputStream getResourceAsStream(TexturePackBase texturepackbase, String s)
    {
        InputStream inputstream = null;
        if(texturepackbase != null)
        {
            try
            {
                inputstream = texturepackbase.getResourceAsStream(s);
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
            }
        }
        if(inputstream == null)
        {
            inputstream = (com.pclewis.mcpatcher.mod.TextureUtils.class).getResourceAsStream(s);
        }
        if(inputstream == null && isRequiredResource(s))
        {
            inputstream = Thread.currentThread().getContextClassLoader().getResourceAsStream(s);
            MCPatcherUtils.warn("falling back on thread class loader for %s: %s", new Object[] {
                s, inputstream != null ? "success" : "failed"
            });
        }
        return inputstream;
    }

    public static InputStream getResourceAsStream(String s)
    {
        return getResourceAsStream(getSelectedTexturePack(), s);
    }

    public static java.awt.image.BufferedImage getResourceAsBufferedImage(TexturePackBase texturepackbase, String s)
        throws IOException
    {
        java.awt.image.BufferedImage bufferedimage = null;
        boolean flag = false;
        if(useTextureCache && texturepackbase == lastTexturePack)
        {
            bufferedimage = (java.awt.image.BufferedImage)cache.get(s);
            if(bufferedimage != null)
            {
                flag = true;
            }
        }
        if(bufferedimage == null)
        {
            InputStream inputstream = getResourceAsStream(texturepackbase, s);
            if(inputstream != null)
            {
                try
                {
                    bufferedimage = ImageIO.read(inputstream);
                }
                finally
                {
                    MCPatcherUtils.close(inputstream);
                }
            }
        }
        if(bufferedimage == null)
        {
            if(isRequiredResource(s))
            {
                throw new IOException((new StringBuilder()).append(s).append(" image is null").toString());
            } else
            {
                return null;
            }
        }
        if(useTextureCache && !flag && texturepackbase != lastTexturePack)
        {
            MCPatcherUtils.log("clearing texture cache (%d items)", new Object[] {
                Integer.valueOf(cache.size())
            });
            cache.clear();
        }
        MCPatcherUtils.log("opened %s %dx%d from %s", new Object[] {
            s, Integer.valueOf(bufferedimage.getWidth()), Integer.valueOf(bufferedimage.getHeight()), flag ? "cache" : getTexturePackName(texturepackbase)
        });
        if(!flag)
        {
            Integer integer;
            if(s.matches("^/custom_\\w+_\\d+\\.png$"))
            {
                integer = Integer.valueOf(1);
            } else
            {
                integer = (Integer)expectedColumns.get(s);
            }
            if(integer != null && bufferedimage.getWidth() != integer.intValue() * TileSize.int_size)
            {
                bufferedimage = resizeImage(bufferedimage, integer.intValue() * TileSize.int_size);
            }
            if(useTextureCache)
            {
                lastTexturePack = texturepackbase;
                cache.put(s, bufferedimage);
            }
            if(s.matches("^/mob/.*_eyes\\d*\\.png$"))
            {
                int i = 0;
                for(int j = 0; j < bufferedimage.getWidth(); j++)
                {
                    for(int k = 0; k < bufferedimage.getHeight(); k++)
                    {
                        int l = bufferedimage.getRGB(j, k);
                        if((l & 0xff000000) == 0 && l != 0)
                        {
                            bufferedimage.setRGB(j, k, 0);
                            i++;
                        }
                    }

                }

                if(i > 0)
                {
                    MCPatcherUtils.log("  fixed %d transparent pixels", new Object[] {
                        Integer.valueOf(i), s
                    });
                }
            }
        }
        return bufferedimage;
    }

    public static java.awt.image.BufferedImage getResourceAsBufferedImage(String s)
        throws IOException
    {
        return getResourceAsBufferedImage(getSelectedTexturePack(), s);
    }

    public static java.awt.image.BufferedImage getResourceAsBufferedImage(Object obj, Object obj1, String s)
        throws IOException
    {
        return getResourceAsBufferedImage(s);
    }

    public static int getTileSize(TexturePackBase texturepackbase)
    {
        int i = 0;
        Iterator iterator = expectedColumns.entrySet().iterator();
        do
        {
            if(!iterator.hasNext())
            {
                break;
            }
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            InputStream inputstream = null;
            try
            {
                try
                {
                    inputstream = getResourceAsStream(texturepackbase, (String)entry.getKey());
                    if(inputstream != null)
                    {
                        java.awt.image.BufferedImage bufferedimage = ImageIO.read(inputstream);
                        int j = bufferedimage.getWidth() / ((Integer)entry.getValue()).intValue();
                        MCPatcherUtils.log("  %s tile size is %d", new Object[] {
                            entry.getKey(), Integer.valueOf(j)
                        });
                        i = Math.max(i, j);
                    }
                }
                catch(Exception exception)
                {
                    exception.printStackTrace();
                }
                continue;
            }
            finally
            {
                MCPatcherUtils.close(inputstream);
            }
        } while(true);
        return i <= 0 ? 16 : i;
    }

    public static int getTileSize()
    {
        return getTileSize(getSelectedTexturePack());
    }

    public static boolean hasResource(TexturePackBase texturepackbase, String s)
    {
        InputStream inputstream = getResourceAsStream(texturepackbase, s);
        boolean flag = inputstream != null;
        MCPatcherUtils.close(inputstream);
        return flag;
    }

    public static boolean hasResource(String s)
    {
        return hasResource(getSelectedTexturePack(), s);
    }

    private static java.awt.image.BufferedImage resizeImage(java.awt.image.BufferedImage bufferedimage, int i)
    {
        int j = (bufferedimage.getHeight() * i) / bufferedimage.getWidth();
        MCPatcherUtils.log("  resizing to %dx%d", new Object[] {
            Integer.valueOf(i), Integer.valueOf(j)
        });
        java.awt.image.BufferedImage bufferedimage1 = new java.awt.image.BufferedImage(i, j, 2);
        java.awt.Graphics2D graphics2d = bufferedimage1.createGraphics();
        graphics2d.drawImage(bufferedimage, 0, 0, i, j, null);
        return bufferedimage1;
    }

    private static void refreshColorizer(int ai[], String s)
    {
        try
        {
            java.awt.image.BufferedImage bufferedimage = getResourceAsBufferedImage(s);
            if(bufferedimage != null)
            {
                bufferedimage.getRGB(0, 0, 256, 256, ai, 0, 256);
            }
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
        }
    }

    public static void openTexturePackFile(TexturePackCustom texturepackcustom)
    {
        if(!autoRefreshTextures || texturepackcustom.texturePackZipFile == null)
        {
            return;
        }
        FileInputStream fileinputstream = null;
        FileOutputStream fileoutputstream = null;
        ZipFile zipfile = null;
        try
        {
            texturepackcustom.lastModified = texturepackcustom.texturePackFile.lastModified();
            texturepackcustom.tmpFile = File.createTempFile("tmpmc", ".zip");
            texturepackcustom.tmpFile.deleteOnExit();
            MCPatcherUtils.close(texturepackcustom.texturePackZipFile);
            fileinputstream = new FileInputStream(texturepackcustom.texturePackFile);
            fileoutputstream = new FileOutputStream(texturepackcustom.tmpFile);
            byte abyte0[] = new byte[0x10000];
            do
            {
                int i = fileinputstream.read(abyte0);
                if(i <= 0)
                {
                    break;
                }
                fileoutputstream.write(abyte0, 0, i);
            } while(true);
            MCPatcherUtils.close(fileinputstream);
            MCPatcherUtils.close(fileoutputstream);
            zipfile = new ZipFile(texturepackcustom.tmpFile);
            texturepackcustom.origZip = texturepackcustom.texturePackZipFile;
            texturepackcustom.texturePackZipFile = zipfile;
            zipfile = null;
            MCPatcherUtils.log("copied %s to %s, lastModified = %d", new Object[] {
                texturepackcustom.texturePackFile.getPath(), texturepackcustom.tmpFile.getPath(), Long.valueOf(texturepackcustom.lastModified)
            });
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
        }
        finally
        {
            MCPatcherUtils.close(fileinputstream);
            MCPatcherUtils.close(fileoutputstream);
            MCPatcherUtils.close(zipfile);
        }
    }

    public static void closeTexturePackFile(TexturePackCustom texturepackcustom)
    {
        if(texturepackcustom.origZip != null)
        {
            MCPatcherUtils.close(texturepackcustom.texturePackZipFile);
            texturepackcustom.texturePackZipFile = texturepackcustom.origZip;
            texturepackcustom.origZip = null;
            texturepackcustom.tmpFile.delete();
            MCPatcherUtils.log("deleted %s", new Object[] {
                texturepackcustom.tmpFile.getPath()
            });
            texturepackcustom.tmpFile = null;
        }
    }

    public static void checkTexturePackChange(Minecraft minecraft)
    {
        if(!autoRefreshTextures || ++textureRefreshCount < 16)
        {
            return;
        }
        textureRefreshCount = 0;
        TexturePackList texturepacklist = minecraft.texturePackList;
        if(!(texturepacklist.selectedTexturePack instanceof TexturePackCustom))
        {
            return;
        }
        TexturePackCustom texturepackcustom = (TexturePackCustom)texturepacklist.selectedTexturePack;
        long l = texturepackcustom.texturePackFile.lastModified();
        if(l == texturepackcustom.lastModified || l == 0L || texturepackcustom.lastModified == 0L)
        {
            return;
        }
        MCPatcherUtils.log("%s lastModified changed from %d to %d", new Object[] {
            texturepackcustom.texturePackFile.getPath(), Long.valueOf(texturepackcustom.lastModified), Long.valueOf(l)
        });
        ZipFile zipfile = null;
        try
        {
            zipfile = new ZipFile(texturepackcustom.texturePackFile);
        }
        catch(IOException ioexception)
        {
            return;
        }
        finally
        {
            MCPatcherUtils.close(zipfile);
        }
        texturepackcustom.closeTexturePackFile();
        texturepacklist.updateAvaliableTexturePacks();
        for(Iterator iterator = texturepacklist.availableTexturePacks().iterator(); iterator.hasNext();)
        {
            TexturePackBase texturepackbase = (TexturePackBase)iterator.next();
            if(texturepackbase instanceof TexturePackCustom)
            {
                TexturePackCustom texturepackcustom1 = (TexturePackCustom)texturepackbase;
                if(texturepackcustom1.texturePackFile.equals(texturepackcustom.texturePackFile))
                {
                    MCPatcherUtils.log("setting new texture pack", new Object[0]);
                    texturepacklist.selectedTexturePack = texturepacklist.defaultTexturePack;
                    texturepacklist.setTexturePack(texturepackcustom1);
                    minecraft.renderEngine.setTileSize(minecraft);
                    return;
                }
            }
        }

        MCPatcherUtils.log("selected texture pack not found after refresh, switching to default", new Object[0]);
        texturepacklist.setTexturePack(texturepacklist.defaultTexturePack);
        minecraft.renderEngine.setTileSize(minecraft);
    }

    static 
    {
        expectedColumns = new HashMap();
        expectedColumns.put("/terrain.png", Integer.valueOf(16));
        expectedColumns.put("/gui/items.png", Integer.valueOf(16));
        expectedColumns.put("/misc/dial.png", Integer.valueOf(1));
        expectedColumns.put("/custom_lava_still.png", Integer.valueOf(1));
        expectedColumns.put("/custom_lava_flowing.png", Integer.valueOf(1));
        expectedColumns.put("/custom_water_still.png", Integer.valueOf(1));
        expectedColumns.put("/custom_water_flowing.png", Integer.valueOf(1));
        expectedColumns.put("/custom_fire_n_s.png", Integer.valueOf(1));
        expectedColumns.put("/custom_fire_e_w.png", Integer.valueOf(1));
        expectedColumns.put("/custom_portal.png", Integer.valueOf(1));
    }
}
