// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package com.pclewis.mcpatcher.mod;

import com.pclewis.mcpatcher.MCPatcherUtils;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import net.minecraft.src.TextureFX;

// Referenced classes of package com.pclewis.mcpatcher.mod:
//            TextureUtils, TileSize

public class CustomAnimation extends TextureFX
{
    private class Strip
        implements Delegate
    {

        private final int oneFrame;
        private final byte src[];
        private final int numFrames;
        private int currentFrame;

        public void onTick()
        {
            if(++currentFrame >= numFrames)
            {
                currentFrame = 0;
            }
            System.arraycopy(src, currentFrame * oneFrame, imageData, 0, oneFrame);
        }

        Strip(BufferedImage bufferedimage)
        {
            oneFrame = TileSize.int_size * TileSize.int_size * 4;
            numFrames = bufferedimage.getHeight() / bufferedimage.getWidth();
            int ai[] = new int[bufferedimage.getWidth() * bufferedimage.getHeight()];
            bufferedimage.getRGB(0, 0, bufferedimage.getWidth(), bufferedimage.getHeight(), ai, 0, TileSize.int_size);
            src = new byte[ai.length * 4];
            CustomAnimation.ARGBtoRGBA(ai, src);
        }
    }

    private class Tile
        implements Delegate
    {

        private final int allButOneRow;
        private final int oneRow;
        private final int minScrollDelay;
        private final int maxScrollDelay;
        private final boolean isScrolling;
        private final byte temp[];
        private int timer;

        public void onTick()
        {
            if(isScrolling && (maxScrollDelay <= 0 || --timer <= 0))
            {
                if(maxScrollDelay > 0)
                {
                    timer = CustomAnimation.rand.nextInt((maxScrollDelay - minScrollDelay) + 1) + minScrollDelay;
                }
                System.arraycopy(imageData, allButOneRow, temp, 0, oneRow);
                System.arraycopy(imageData, 0, imageData, oneRow, allButOneRow);
                System.arraycopy(temp, 0, imageData, 0, oneRow);
            }
        }

        Tile(String s, int i, int j, int k)
        {
            oneRow = TileSize.int_size * 4;
            allButOneRow = (TileSize.int_size - 1) * oneRow;
            minScrollDelay = j;
            maxScrollDelay = k;
            isScrolling = minScrollDelay >= 0;
            if(isScrolling)
            {
                temp = new byte[oneRow];
            } else
            {
                temp = null;
            }
            BufferedImage bufferedimage;
            try
            {
                bufferedimage = TextureUtils.getResourceAsBufferedImage(s);
            }
            catch(IOException ioexception)
            {
                ioexception.printStackTrace();
                return;
            }
            int l = (i % 16) * TileSize.int_size;
            int i1 = (i / 16) * TileSize.int_size;
            int ai[] = new int[TileSize.int_numPixels];
            bufferedimage.getRGB(l, i1, TileSize.int_size, TileSize.int_size, ai, 0, TileSize.int_size);
            CustomAnimation.ARGBtoRGBA(ai, imageData);
        }
    }

    private static interface Delegate
    {

        public abstract void onTick();
    }


    private Delegate _flddelegate;
    private static Random rand = new Random();

    public CustomAnimation(int i, int j, int k, String s, int l, int i1)
    {
        super(i);
        iconIndex = i;
        tileImage = j;
        tileSize = k;
        BufferedImage bufferedimage = null;
        String s1 = j != 0 ? "/gui/items.png" : "/terrain.png";
        try
        {
            String s2 = (new StringBuilder()).append("/custom_").append(s).append(".png").toString();
            bufferedimage = TextureUtils.getResourceAsBufferedImage(s2);
            if(bufferedimage != null)
            {
                s1 = s2;
            }
        }
        catch(IOException ioexception) { }
        MCPatcherUtils.log("new CustomAnimation %s, src=%s, buffer size=0x%x, tile=%d", new Object[] {
            s, s1, Integer.valueOf(imageData.length), Integer.valueOf(iconIndex)
        });
        if(bufferedimage == null)
        {
            _flddelegate = new Tile(s1, i, l, i1);
        } else
        {
            _flddelegate = new Strip(bufferedimage);
        }
    }

    private static void ARGBtoRGBA(int ai[], byte abyte0[])
    {
        for(int i = 0; i < ai.length; i++)
        {
            int j = ai[i];
            abyte0[i * 4 + 3] = (byte)(j >> 24 & 0xff);
            abyte0[i * 4 + 0] = (byte)(j >> 16 & 0xff);
            abyte0[i * 4 + 1] = (byte)(j >> 8 & 0xff);
            abyte0[i * 4 + 2] = (byte)(j >> 0 & 0xff);
        }

    }

    public void onTick()
    {
        _flddelegate.onTick();
    }



}
