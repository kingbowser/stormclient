// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package com.pclewis.mcpatcher;

import java.io.*;
import java.util.zip.ZipFile;
import net.minecraft.client.Minecraft;

// Referenced classes of package com.pclewis.mcpatcher:
//            Config

public class MCPatcherUtils
{

    private static File minecraftDir;
    private static boolean debug = false;
    private static boolean isGame;
    static Config config = null;
    private static Minecraft minecraft;
    private static String minecraftVersion;
    private static String patcherVersion;
    public static final String HD_TEXTURES = "HD Textures";
    public static final String HD_FONT = "HD Font";
    public static final String BETTER_GRASS = "Better Grass";
    public static final String RANDOM_MOBS = "Random Mobs";
    public static final String GLSL_SHADERS = "GLSL Shaders";
    public static final String UTILS_CLASS = "com.pclewis.mcpatcher.MCPatcherUtils";
    public static final String CONFIG_CLASS = "com.pclewis.mcpatcher.Config";
    public static final String TILE_SIZE_CLASS = "com.pclewis.mcpatcher.mod.TileSize";
    public static final String TEXTURE_UTILS_CLASS = "com.pclewis.mcpatcher.mod.TextureUtils";
    public static final String CUSTOM_ANIMATION_CLASS = "com.pclewis.mcpatcher.mod.CustomAnimation";
    public static final String RANDOM_MOBS_CLASS = "com.pclewis.mcpatcher.mod.MobRandomizer";
    public static final String SHADERS_CLASS = "com.pclewis.mcpatcher.mod.Shaders";

    private MCPatcherUtils()
    {
    }

    static File getDefaultGameDir()
    {
        String s = System.getProperty("os.name").toLowerCase();
        String s1 = null;
        String s2 = ".minecraft";
        if(s.contains("win"))
        {
            s1 = System.getenv("APPDATA");
        } else
        if(s.contains("mac"))
        {
            s2 = "Library/Application Support/minecraft";
        }
        if(s1 == null)
        {
            s1 = System.getProperty("user.home");
        }
        return new File(s1, s2);
    }

    static boolean setGameDir(File file)
    {
        if(file != null && file.isDirectory() && (new File(file, "bin/lwjgl.jar")).exists() && (new File(file, "resources")).isDirectory())
        {
            minecraftDir = file.getAbsoluteFile();
        } else
        {
            minecraftDir = null;
        }
        return loadProperties();
    }

    private static boolean loadProperties()
    {
        config = null;
        if(minecraftDir != null && minecraftDir.exists())
        {
            try
            {
                config = new Config(minecraftDir);
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
            }
            debug = getBoolean("debug", false);
            return true;
        } else
        {
            return false;
        }
    }

    public static File getMinecraftPath(String as[])
    {
        File file = minecraftDir;
        String as1[] = as;
        int i = as1.length;
        for(int j = 0; j < i; j++)
        {
            String s = as1[j];
            file = new File(file, s);
        }

        return file;
    }

    public static void log(String s, Object aobj[])
    {
        if(debug)
        {
            System.out.printf((new StringBuilder()).append(s).append("\n").toString(), aobj);
        }
    }

    public static boolean isGame()
    {
        return isGame;
    }

    public static void warn(String s, Object aobj[])
    {
        System.out.printf((new StringBuilder()).append("WARNING: ").append(s).append("\n").toString(), aobj);
    }

    public static void error(String s, Object aobj[])
    {
        System.out.printf((new StringBuilder()).append("ERROR: ").append(s).append("\n").toString(), aobj);
    }

    public static String getString(String s, String s1, Object obj)
    {
        if(config == null)
        {
            return obj != null ? obj.toString() : null;
        }
        String s2 = config.getModConfigValue(s, s1);
        if(s2 == null && obj != null)
        {
            s2 = obj.toString();
            config.setModConfigValue(s, s1, s2);
        }
        return s2;
    }

    public static String getString(String s, Object obj)
    {
        if(config == null)
        {
            return obj != null ? obj.toString() : null;
        }
        String s1 = config.getConfigValue(s);
        if(s1 == null && obj != null)
        {
            s1 = obj.toString();
            config.setConfigValue(s, s1);
        }
        return s1;
    }

    public static int getInt(String s, String s1, int i)
    {
        int j;
        try
        {
            j = Integer.parseInt(getString(s, s1, Integer.valueOf(i)));
        }
        catch(NumberFormatException numberformatexception)
        {
            j = i;
        }
        return j;
    }

    public static int getInt(String s, int i)
    {
        int j;
        try
        {
            j = Integer.parseInt(getString(s, Integer.valueOf(i)));
        }
        catch(NumberFormatException numberformatexception)
        {
            j = i;
        }
        return j;
    }

    public static boolean getBoolean(String s, String s1, boolean flag)
    {
        String s2 = getString(s, s1, Boolean.valueOf(flag)).toLowerCase();
        if(s2.equals("false"))
        {
            return false;
        }
        if(s2.equals("true"))
        {
            return true;
        } else
        {
            return flag;
        }
    }

    public static boolean getBoolean(String s, boolean flag)
    {
        String s1 = getString(s, Boolean.valueOf(flag)).toLowerCase();
        if(s1.equals("false"))
        {
            return false;
        }
        if(s1.equals("true"))
        {
            return true;
        } else
        {
            return flag;
        }
    }

    public static void set(String s, String s1, Object obj)
    {
        if(config != null)
        {
            config.setModConfigValue(s, s1, obj.toString());
        }
    }

    static void set(String s, Object obj)
    {
        if(config != null)
        {
            config.setConfigValue(s, obj.toString());
        }
    }

    public static void remove(String s, String s1)
    {
        if(config != null)
        {
            config.remove(config.getModConfig(s, s1));
        }
    }

    static void remove(String s)
    {
        if(config != null)
        {
            config.remove(config.getConfig(s));
        }
    }

    public static void close(Closeable closeable)
    {
        if(closeable != null)
        {
            try
            {
                closeable.close();
            }
            catch(IOException ioexception)
            {
                ioexception.printStackTrace();
            }
        }
    }

    public static void close(ZipFile zipfile)
    {
        if(zipfile != null)
        {
            try
            {
                zipfile.close();
            }
            catch(IOException ioexception)
            {
                ioexception.printStackTrace();
            }
        }
    }

    public static void setMinecraft(Minecraft minecraft1)
    {
        minecraft = minecraft1;
    }

    public static void setVersions(String s, String s1)
    {
        minecraftVersion = s;
        patcherVersion = s1;
    }

    public static Minecraft getMinecraft()
    {
        return minecraft;
    }

    public static String getMinecraftVersion()
    {
        return minecraftVersion;
    }

    public static String getPatcherVersion()
    {
        return patcherVersion;
    }

    static 
    {
        minecraftDir = null;
        isGame = true;
        try
        {
            if(Class.forName("com.pclewis.mcpatcher.MCPatcher") != null)
            {
                isGame = false;
            }
        }
        catch(ClassNotFoundException classnotfoundexception) { }
        catch(Throwable throwable)
        {
            throwable.printStackTrace();
        }
        if(isGame)
        {
            if(setGameDir(new File(".")) || setGameDir(getDefaultGameDir()))
            {
                System.out.println((new StringBuilder()).append("MCPatcherUtils initialized. Directory ").append(minecraftDir.getPath()).toString());
            } else
            {
                System.out.println((new StringBuilder()).append("MCPatcherUtils initialized. Current directory ").append((new File(".")).getAbsolutePath()).toString());
            }
        }
    }
}
