// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode fieldsfirst 

package com.pclewis.mcpatcher;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.*;

// Referenced classes of package com.pclewis.mcpatcher:
//            MCPatcherUtils

class Config
{

    private File xmlFile;
    Document xml;
    Element selectedProfile;
    static final String TAG_ROOT = "mcpatcherProfile";
    static final String TAG_CONFIG1 = "config";
    static final String TAG_SELECTED_PROFILE = "selectedProfile";
    static final String TAG_LAST_MOD_DIRECTORY = "lastModDirectory";
    static final String TAG_DEBUG = "debug";
    static final String TAG_JAVA_HEAP_SIZE = "javaHeapSize";
    static final String TAG_LAST_VERSION = "lastVersion";
    static final String TAG_BETA_WARNING_SHOWN = "betaWarningShown";
    static final String TAG_MODS = "mods";
    static final String ATTR_PROFILE = "profile";
    static final String TAG_MOD = "mod";
    static final String TAG_NAME = "name";
    static final String TAG_TYPE = "type";
    static final String TAG_PATH = "path";
    static final String TAG_FILES = "files";
    static final String TAG_FILE = "file";
    static final String TAG_FROM = "from";
    static final String TAG_TO = "to";
    static final String TAG_CLASS = "class";
    static final String TAG_ENABLED = "enabled";
    static final String ATTR_VERSION = "version";
    static final String VAL_BUILTIN = "builtIn";
    static final String VAL_EXTERNAL_ZIP = "externalZip";
    static final String VAL_EXTERNAL_JAR = "externalJar";
    private static final int XML_INDENT_AMOUNT = 2;
    private static final String XSLT_REFORMAT = "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output method=\"xml\" omit-xml-declaration=\"no\"/><xsl:strip-space elements=\"*\"/><xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template></xsl:stylesheet>";

    Config(File file)
        throws ParserConfigurationException
    {
        xmlFile = null;
        xmlFile = new File(file, "mcpatcher.xml");
        DocumentBuilderFactory documentbuilderfactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentbuilder = documentbuilderfactory.newDocumentBuilder();
        boolean flag = false;
        if(xmlFile.exists())
        {
            try
            {
                xml = documentbuilder.parse(xmlFile);
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
            }
        }
        if(xml == null)
        {
            xml = documentbuilder.newDocument();
            buildNewProperties();
            flag = true;
        }
        if(flag)
        {
            saveProperties();
        }
    }

    Element getElement(Element element, String s)
    {
        if(element == null)
        {
            return null;
        }
        NodeList nodelist = element.getElementsByTagName(s);
        Element element1;
        if(nodelist.getLength() == 0)
        {
            element1 = xml.createElement(s);
            element.appendChild(element1);
        } else
        {
            element1 = (Element)nodelist.item(0);
        }
        return element1;
    }

    String getText(Node node)
    {
        if(node == null)
        {
            return null;
        }
        switch(node.getNodeType())
        {
        case 3: // '\003'
            return ((Text)node).getData();

        case 2: // '\002'
            return ((Attr)node).getValue();

        case 1: // '\001'
            NodeList nodelist = node.getChildNodes();
            for(int i = 0; i < nodelist.getLength(); i++)
            {
                Node node1 = nodelist.item(i);
                if(node1.getNodeType() == 3)
                {
                    return ((Text)node1).getData();
                }
            }

            break;
        }
        return null;
    }

    void setText(Element element, String s, String s1)
    {
        if(element == null)
        {
            return;
        }
        Element element1;
        for(element1 = getElement(element, s); element1.hasChildNodes(); element1.removeChild(element1.getFirstChild())) { }
        Text text = xml.createTextNode(s1);
        element1.appendChild(text);
    }

    void remove(Node node)
    {
        if(node != null)
        {
            Node node1 = node.getParentNode();
            node1.removeChild(node);
        }
    }

    String getText(Element element, String s)
    {
        return getText(((Node) (getElement(element, s))));
    }

    Element getRoot()
    {
        if(xml == null)
        {
            return null;
        }
        Element element = xml.getDocumentElement();
        if(element == null)
        {
            element = xml.createElement("mcpatcherProfile");
            xml.appendChild(element);
        }
        return element;
    }

    Element getConfig()
    {
        return getElement(getRoot(), "config");
    }

    Element getConfig(String s)
    {
        return getElement(getConfig(), s);
    }

    String getConfigValue(String s)
    {
        return getText(getConfig(s));
    }

    void setConfigValue(String s, String s1)
    {
        Element element = getConfig(s);
        if(element != null)
        {
            for(; element.hasChildNodes(); element.removeChild(element.getFirstChild())) { }
            element.appendChild(xml.createTextNode(s1));
        }
    }

    static String getDefaultProfileName(String s)
    {
        return (new StringBuilder()).append("Minecraft ").append(s).toString();
    }

    static boolean isDefaultProfile(String s)
    {
        return s.startsWith("Minecraft ");
    }

    void setDefaultProfileName(String s)
    {
        Element element = getRoot();
        NodeList nodelist = element.getElementsByTagName("mods");
        String s1 = getConfigValue("selectedProfile");
        if(s1 == null || s1.equals(""))
        {
            setConfigValue("selectedProfile", s);
        }
        boolean flag = false;
        for(int i = 0; i < nodelist.getLength(); i++)
        {
            Node node = nodelist.item(i);
            if(!(node instanceof Element))
            {
                continue;
            }
            Element element1 = (Element)node;
            String s2 = element1.getAttribute("profile");
            if(s2 != null && !s2.equals(""))
            {
                continue;
            }
            if(flag)
            {
                element.removeChild(element1);
            } else
            {
                element1.setAttribute("profile", s);
                flag = true;
            }
        }

    }

    Element findProfileByName(String s, boolean flag)
    {
        Element element = null;
        Element element1 = getRoot();
        NodeList nodelist = element1.getElementsByTagName("mods");
        for(int i = 0; i < nodelist.getLength(); i++)
        {
            Node node = nodelist.item(i);
            if(!(node instanceof Element))
            {
                continue;
            }
            Element element2 = (Element)node;
            String s1 = element2.getAttribute("profile");
            if(s == null || s.equals(s1))
            {
                return element2;
            }
        }

        if(flag)
        {
            element = xml.createElement("mods");
            if(selectedProfile != null)
            {
                NodeList nodelist1 = selectedProfile.getElementsByTagName("mod");
                for(int j = 0; j < nodelist1.getLength(); j++)
                {
                    Node node1 = nodelist1.item(j);
                    if(!(node1 instanceof Element))
                    {
                        continue;
                    }
                    Element element3 = (Element)node1;
                    String s2 = getText(element3, "type");
                    if("builtIn".equals(s2))
                    {
                        element.appendChild(node1.cloneNode(true));
                    }
                }

            }
            element.setAttribute("profile", s);
            element1.appendChild(element);
        }
        return element;
    }

    void selectProfile()
    {
        selectProfile(getConfigValue("selectedProfile"));
    }

    void selectProfile(String s)
    {
        selectedProfile = findProfileByName(s, true);
        setConfigValue("selectedProfile", s);
    }

    void deleteProfile(String s)
    {
        Element element = getRoot();
        Element element1 = findProfileByName(s, false);
        if(element1 != null)
        {
            if(element1 == selectedProfile)
            {
                selectedProfile = null;
            }
            element.removeChild(element1);
        }
        getMods();
    }

    void renameProfile(String s, String s1)
    {
        if(!s.equals(s1))
        {
            Element element = findProfileByName(s, false);
            if(element != null)
            {
                element.setAttribute("profile", s1);
                String s2 = getConfigValue("selectedProfile");
                if(s.equals(s2))
                {
                    setConfigValue("selectedProfile", s1);
                }
            }
        }
    }

    void rewriteModPaths(File file, File file1)
    {
        NodeList nodelist = getRoot().getElementsByTagName("mods");
        for(int i = 0; i < nodelist.getLength(); i++)
        {
            Element element = (Element)nodelist.item(i);
            rewriteModPaths(element, file, file1);
        }

    }

    void rewriteModPaths(Element element, File file, File file1)
    {
        NodeList nodelist = element.getElementsByTagName("mod");
        for(int i = 0; i < nodelist.getLength(); i++)
        {
            Element element1 = (Element)nodelist.item(i);
            String s = getText(element1, "type");
            if(!"externalZip".equals(s))
            {
                continue;
            }
            String s1 = getText(element1, "path");
            if(s1 == null || s1.equals(""))
            {
                continue;
            }
            File file2 = new File(s1);
            if(file.equals(file2.getParentFile()))
            {
                setText(element1, "path", (new File(file1, file2.getName())).getPath());
            }
        }

    }

    ArrayList getProfiles()
    {
        ArrayList arraylist = new ArrayList();
        Element element = getRoot();
        NodeList nodelist = element.getElementsByTagName("mods");
        for(int i = 0; i < nodelist.getLength(); i++)
        {
            Node node = nodelist.item(i);
            if(!(node instanceof Element))
            {
                continue;
            }
            Element element1 = (Element)node;
            String s = element1.getAttribute("profile");
            if(s != null && !s.equals(""))
            {
                arraylist.add(s);
            }
        }

        Collections.sort(arraylist);
        return arraylist;
    }

    Element getMods()
    {
        if(selectedProfile == null)
        {
            selectProfile();
        }
        return selectedProfile;
    }

    boolean hasMod(String s)
    {
        Element element = getMods();
        if(element != null)
        {
            NodeList nodelist = element.getElementsByTagName("mod");
            for(int i = 0; i < nodelist.getLength(); i++)
            {
                Element element1 = (Element)nodelist.item(i);
                NodeList nodelist1 = element1.getElementsByTagName("name");
                if(nodelist1.getLength() <= 0)
                {
                    continue;
                }
                element1 = (Element)nodelist1.item(0);
                if(s.equals(getText(element1)))
                {
                    return true;
                }
            }

        }
        return false;
    }

    Element getMod(String s)
    {
        Element element = getMods();
        if(element == null)
        {
            return null;
        }
        NodeList nodelist = element.getElementsByTagName("mod");
        for(int i = 0; i < nodelist.getLength(); i++)
        {
            Node node = nodelist.item(i);
            if(!(node instanceof Element))
            {
                continue;
            }
            Element element3 = (Element)node;
            if(s.equals(getText(element3, "name")))
            {
                return element3;
            }
        }

        Element element1 = xml.createElement("mod");
        element.appendChild(element1);
        Element element2 = xml.createElement("name");
        Text text = xml.createTextNode(s);
        element2.appendChild(text);
        element1.appendChild(element2);
        element2 = xml.createElement("enabled");
        element1.appendChild(element2);
        element2 = xml.createElement("type");
        element1.appendChild(element2);
        return element1;
    }

    void setModEnabled(String s, boolean flag)
    {
        setText(getMod(s), "enabled", Boolean.toString(flag));
    }

    Element getModConfig(String s)
    {
        return getElement(getMod(s), "config");
    }

    Element getModConfig(String s, String s1)
    {
        return getElement(getModConfig(s), s1);
    }

    String getModConfigValue(String s, String s1)
    {
        return getText(getModConfig(s, s1));
    }

    void setModConfigValue(String s, String s1, String s2)
    {
        Element element = getModConfig(s, s1);
        if(element != null)
        {
            for(; element.hasChildNodes(); element.removeChild(element.getFirstChild())) { }
            element.appendChild(xml.createTextNode(s2));
        }
    }

    private void buildNewProperties()
    {
        if(xml != null)
        {
            getRoot();
            getConfig();
            if(selectedProfile != null)
            {
                getMods();
                setText(getMod("HD Textures"), "enabled", "true");
                setText(getMod("HD Font"), "enabled", "true");
                setText(getMod("Better Grass"), "enabled", "false");
            }
        }
    }

    boolean saveProperties()
    {
        boolean flag = false;
        if(xml != null && xmlFile != null)
        {
            FileOutputStream fileoutputstream = null;
            try
            {
                TransformerFactory transformerfactory = TransformerFactory.newInstance();
                Transformer transformer;
                try
                {
                    transformerfactory.setAttribute("indent-number", Integer.valueOf(2));
                    transformer = transformerfactory.newTransformer(new StreamSource(new StringReader("<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output method=\"xml\" omit-xml-declaration=\"no\"/><xsl:strip-space elements=\"*\"/><xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template></xsl:stylesheet>")));
                    transformer.setOutputProperty("indent", "yes");
                    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                }
                catch(Throwable throwable)
                {
                    transformer = transformerfactory.newTransformer();
                }
                DOMSource domsource = new DOMSource(xml);
                fileoutputstream = new FileOutputStream(xmlFile);
                transformer.transform(domsource, new StreamResult(new OutputStreamWriter(fileoutputstream, "UTF-8")));
                flag = true;
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
            }
            finally
            {
                MCPatcherUtils.close(fileoutputstream);
            }
        }
        return flag;
    }
}
